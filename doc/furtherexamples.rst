.. title:: Further examples

.. toctree::
   :maxdepth: 1

   discontinuousgalerkin_nb
   elasticity_nb
   spiral_nb
   wave_nb
   stokes
   evalues_laplace_nb
   biharmonic_IPC0_nb
   crystal_nb
   mcf_nb
   laplace-dwr_nb

.. todo:: Give some details on the Uzawa algorithm, add dune-fempy version and an algorithm implementation
.. todo:: Add an example using mass lumping to show how to change quadrature
