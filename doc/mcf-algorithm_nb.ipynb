{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "bea1e4c9",
   "metadata": {},
   "source": [
    "# Mean Curvature Flow (revisited)\n",
    "\n",
    "[As discussed before](mcf_nb.ipynb)\n",
    "we can simulate the shrinking of a sphere under mean curvature flow\n",
    "using a finite element approach based on\n",
    "the following time discrete approximation:\n",
    "\\begin{align}\n",
    "\\int_{\\Gamma^n} \\big( U^{n+1} - {\\rm id}\\big) \\cdot \\varphi +\n",
    "\\tau \\int_{\\Gamma^n} \\big(\n",
    "\\theta\\nabla_{\\Gamma^n} U^{n+1} + (1-\\theta) I \\big)\n",
    "\\colon\\nabla_{\\Gamma^n}\\varphi\n",
    "=0~.\n",
    "\\end{align}\n",
    "Here $U^n$ parametrizes $\\Gamma(t^{n+1})$ over\n",
    "$\\Gamma^n:=\\Gamma(t^{n})$,\n",
    "$I$ is the identity matrix, $\\tau$ is the time step and\n",
    "$\\theta\\in[0,1]$ is a discretization parameter.\n",
    "\n",
    "If the initial surface $\\Gamma^0$ is a sphere of radius $R_0$,\n",
    "the surface remains sphere and we have an exact formula for the evolution\n",
    "of the radius of the surface\n",
    "\n",
    "$$R(t) = \\sqrt{R_0^2 - 4t}.$$\n",
    "\n",
    "To compare the accuracy of the surface approximation we compute an\n",
    "average radius of the discrete surface in each time step $t^n$ using\n",
    "\n",
    "$$R_h^n = \\frac{ \\int_{\\Gamma^n} |x| }{ |\\Gamma^n| }.$$\n",
    "\n",
    "Computing $R_h^n$ requires a grid traversal and a number of calls to\n",
    "interface methods on each element. Doing this on the Python side has a\n",
    "potential performance impact which we investigate here by comparing a\n",
    "pure python implementation with a hybrid approach where computing $R_h^n$\n",
    "is implemented in C++ using the `dune.generator.algorithm` functionality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "4012517d",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:46:42.195505Z",
     "iopub.status.busy": "2024-12-16T11:46:42.195038Z",
     "iopub.status.idle": "2024-12-16T11:46:43.180519Z",
     "shell.execute_reply": "2024-12-16T11:46:43.179633Z"
    }
   },
   "outputs": [],
   "source": [
    "import time, io\n",
    "import pickle\n",
    "import numpy\n",
    "from matplotlib import pyplot as plt\n",
    "from matplotlib.ticker import ScalarFormatter\n",
    "\n",
    "from ufl import *\n",
    "import dune.ufl\n",
    "from dune.generator import algorithm\n",
    "import dune.geometry as geometry\n",
    "import dune.fem as fem\n",
    "\n",
    "def plot(ct, ct2):\n",
    "    fig, ax = plt.subplots()\n",
    "    plt.loglog(ct[0][:], ct[1][:],'*-', markersize=15, label='hybrid')\n",
    "    plt.loglog(ct2[0][:], ct2[1][:],'*-', markersize=15, label='python')\n",
    "    plt.grid(True)\n",
    "    for axis in [ax.xaxis, ax.yaxis]:\n",
    "        axis.set_major_formatter(ScalarFormatter())\n",
    "        axis.set_minor_formatter(ScalarFormatter())\n",
    "    yticks = ax.yaxis.get_minor_ticks()\n",
    "    for t in yticks:\n",
    "        t.label1.set_visible(False)\n",
    "    xticks = ax.xaxis.get_minor_ticks()\n",
    "    for t in xticks:\n",
    "        t.label1.set_visible(False)\n",
    "    plt.yticks(numpy.append(ct[1], ct2[1]))\n",
    "    plt.xticks(ct[0])\n",
    "    plt.legend(loc=\"upper left\")\n",
    "    plt.xlabel('Number of Grid Elements (log)',fontsize=20)\n",
    "    plt.gcf().subplots_adjust(bottom=0.17, left=0.16)\n",
    "    plt.ylabel('Runtime in s (log)',fontsize=20)\n",
    "\n",
    "# polynomial order of surface approximation\n",
    "order = 2\n",
    "\n",
    "# initial radius\n",
    "R0 = 2.\n",
    "\n",
    "# end time\n",
    "endTime = 0.1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d25a9d8e",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Main function for calculating the mean curvature flow of a given surface.\n",
    "If first argument is `True` the radius of the computed surface is\n",
    "computed using an algorithm implemented in C++ otherwise the computation\n",
    "is done in Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "bd64a7c9",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:46:43.183941Z",
     "iopub.status.busy": "2024-12-16T11:46:43.183579Z",
     "iopub.status.idle": "2024-12-16T11:46:43.188374Z",
     "shell.execute_reply": "2024-12-16T11:46:43.187776Z"
    }
   },
   "outputs": [],
   "source": [
    "def calcRadius(surface):\n",
    "    R,vol = 0, 0\n",
    "    for e in surface.elements:\n",
    "        rule = geometry.quadratureRule(e.type, 4)\n",
    "        for p in rule:\n",
    "            geo = e.geometry\n",
    "            weight = geo.volume * p.weight\n",
    "            R   += geo.toGlobal(p.position).two_norm * weight\n",
    "            vol += weight\n",
    "    return R/vol\n",
    "\n",
    "code = \"\"\"\n",
    "#include <dune/geometry/quadraturerules.hh>\n",
    "template< class Surface >\n",
    "double calcRadius( const Surface &surface ) {\n",
    "  double R = 0, vol = 0.;\n",
    "  for( const auto &entity : elements( surface ) ) {\n",
    "    const auto& rule = Dune::QuadratureRules<double, 2>::rule(entity.type(), 4);\n",
    "    for ( const auto &p : rule ) {\n",
    "      const auto geo = entity.geometry();\n",
    "      const double weight = geo.volume() * p.weight();\n",
    "      R   += geo.global(p.position()).two_norm() * weight;\n",
    "      vol += weight;\n",
    "    }\n",
    "  }\n",
    "  return R/vol;\n",
    "}\n",
    "\"\"\"\n",
    "switchCalcRadius = lambda use_cpp,surface: \\\n",
    "             algorithm.load('calcRadius', io.StringIO(code), surface) \\\n",
    "             if use_cpp else calcRadius"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa2e83c2",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Timings for a number of different grid refinements is dumped to disk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "94cfb0ef",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:46:43.193007Z",
     "iopub.status.busy": "2024-12-16T11:46:43.192783Z",
     "iopub.status.idle": "2024-12-16T11:46:43.202923Z",
     "shell.execute_reply": "2024-12-16T11:46:43.202030Z"
    }
   },
   "outputs": [],
   "source": [
    "from dune.fem.view import geometryGridView as geoGridView\n",
    "from dune.fem.space import lagrange as solutionSpace\n",
    "from dune.fem.scheme import galerkin as solutionScheme\n",
    "def calculate(use_cpp, gridView):\n",
    "    # space on Gamma_0 to describe position of Gamma(t)\n",
    "    space = solutionSpace(gridView, dimRange=gridView.dimWorld, order=order)\n",
    "    u = TrialFunction(space)\n",
    "    v = TestFunction(space)\n",
    "    x = SpatialCoordinate(space)\n",
    "    positions = space.interpolate(x, name=\"position\")\n",
    "\n",
    "    # space for discrete solution on Gamma(t)\n",
    "    surface = geoGridView(positions)\n",
    "    space = solutionSpace(surface, dimRange=surface.dimWorld, order=order)\n",
    "    solution  = space.interpolate(x, name=\"solution\")\n",
    "\n",
    "    # set up model using theta scheme\n",
    "    theta = 0.5   # Crank-Nicolson\n",
    "\n",
    "    I = Identity(3)\n",
    "    dt = dune.ufl.Constant(0,\"dt\")\n",
    "\n",
    "    a = (inner(u - x, v) + dt * inner(theta*grad(u)\n",
    "        + (1 - theta)*I, grad(v))) * dx\n",
    "\n",
    "    scheme = solutionScheme(a == 0, space, solver=\"cg\")\n",
    "\n",
    "    Rexact = lambda t: numpy.sqrt(R0*R0 - 4.*t)\n",
    "    radius = switchCalcRadius(use_cpp,surface)\n",
    "\n",
    "    dt.value = 0.02\n",
    "\n",
    "    numberOfLoops = 3\n",
    "    times = numpy.zeros(numberOfLoops)\n",
    "    errors = numpy.zeros(numberOfLoops)\n",
    "    totalIterations = numpy.zeros(numberOfLoops, numpy.dtype(numpy.uint32))\n",
    "    gridSizes = numpy.zeros(numberOfLoops, numpy.dtype(numpy.uint32))\n",
    "    for i in range(numberOfLoops):\n",
    "        positions.interpolate(x * (R0/sqrt(dot(x,x))))\n",
    "        solution.interpolate(x)\n",
    "        t = 0.\n",
    "        error = abs(radius(surface)-Rexact(t))\n",
    "        iterations = 0\n",
    "        start = time.time()\n",
    "        while t < endTime:\n",
    "            info = scheme.solve(target=solution)\n",
    "            # move the surface\n",
    "            positions.assign(solution)\n",
    "            # store some information about the solution process\n",
    "            iterations += int( info[\"linear_iterations\"] )\n",
    "            t          += scheme.model.dt\n",
    "            error       = max(error, abs(radius(surface)-Rexact(t)))\n",
    "        print(\"time used:\", time.time() - start)\n",
    "        times[i] = time.time() - start\n",
    "        errors[i] = error\n",
    "        totalIterations[i] = iterations\n",
    "        gridSizes[i] = gridView.size(2)\n",
    "        if i < numberOfLoops - 1:\n",
    "            gridView.hierarchicalGrid.globalRefine(1)\n",
    "            dt.value /= 2\n",
    "    eocs = numpy.log(errors[0:][:numberOfLoops-1] / errors[1:]) / numpy.log(numpy.sqrt(2))\n",
    "    try:\n",
    "        import pandas as pd\n",
    "        keys = {'size': gridSizes, 'error': errors, \"eoc\": numpy.insert(eocs, 0, None), 'iterations': totalIterations}\n",
    "        table = pd.DataFrame(keys, index=range(numberOfLoops),columns=['size', 'error', 'eoc', 'iterations'])\n",
    "        print(table)\n",
    "    except ImportError:\n",
    "        print(\"pandas module not found so not showing table - ignored\")\n",
    "        pass\n",
    "    return gridSizes, times"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3333fbf3",
   "metadata": {},
   "source": [
    "Compute the mean curvature flow evolution of a spherical surface. Compare\n",
    "computational time of a pure Python implementation and using a C++\n",
    "algorithm to compute the radius of the surface for verifying the\n",
    "algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "04f9076c",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:46:43.207278Z",
     "iopub.status.busy": "2024-12-16T11:46:43.207043Z",
     "iopub.status.idle": "2024-12-16T11:50:15.996824Z",
     "shell.execute_reply": "2024-12-16T11:50:15.995744Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "time used: 0.3224499225616455\n",
      "time used: 1.8327796459197998\n",
      "time used: 8.089809656143188\n",
      "   size     error       eoc  iterations\n",
      "0   318  0.001060       NaN          76\n",
      "1   766  0.000605  1.619153         208\n",
      "2  1745  0.000275  2.275142         420\n",
      "time used: 0.47499561309814453\n",
      "time used: 2.6741676330566406\n",
      "time used: 11.142962455749512\n",
      "   size     error       eoc  iterations\n",
      "0   318  0.001060       NaN          76\n",
      "1   766  0.000605  1.619153         208\n",
      "2  1745  0.000275  2.275142         420\n"
     ]
    }
   ],
   "source": [
    "# set up reference domain Gamma_0\n",
    "results = []\n",
    "from dune.alugrid import aluConformGrid as leafGridView\n",
    "gridView = leafGridView(\"sphere.dgf\", dimgrid=2, dimworld=3)\n",
    "results += [calculate(True, gridView)]\n",
    "\n",
    "gridView = leafGridView(\"sphere.dgf\", dimgrid=2, dimworld=3)\n",
    "results += [calculate(False, gridView)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3538ae22",
   "metadata": {},
   "source": [
    "Compare the hybrid and pure Python versions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "859d0d43",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-16T11:50:16.001733Z",
     "iopub.status.busy": "2024-12-16T11:50:16.001271Z",
     "iopub.status.idle": "2024-12-16T11:50:16.365788Z",
     "shell.execute_reply": "2024-12-16T11:50:16.364862Z"
    }
   },
   "outputs": [
    {
     "data": {
      "image/jpeg": "/9j/4AAQSkZJRgABAQEAMgAyAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADQASADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAopG+6eSOOorlNK1TUHvrJbh9UeJjdpJ52nsgIWX90zERjBKfQH0zQB1lQXt0ljYXF5IrMkETSsqYyQoJIGe/FYk2oakNU1OOzivLgG1t5LaOS22IjszB9rsFBIUoxUtnqBjnEVxdXz/Dd5r22uXvX01hOjqiSBjGdzMMgDnnA/LtQBq/2nd/8AQD1D/vuD/wCO0i6rdMWA0PUMqcH54PTP/PX3p13eR20Ms81rNI27aiLC0pztyAQgYge+K89n+IN3H4ghQ6FJEFQxvZN993bG0/dzngYGO59a2pUJ1PhRjVrwpW5meg/2nd/9APUP++4P/jtH9p3f/QD1D/vuD/47TNJvo9TtQ76dLbTBQZIpYHTaTngFlXd05xWHc+KZbG8kim0eKaIzTRxPC/zHYURQVx1Z5I1BB/iycAVk007M1TUldG//AGnd/wDQD1D/AL7g/wDjtH9p3f8A0A9Q/wC+4P8A47XP2/imWW7Nn/ZllLcG7lgHlz4CBZmQeYNpKkgZHXdhjx0p+n+Jnvr7TLU2NpH9ocLLlvnI8hpMon93IA3EnkMMd6Qzd/tO7/6Aeof99wf/AB2j+07v/oB6h/33B/8AHatH91MvlwnlTkJgenvT/Of/AJ9pfzX/ABoApf2nd/8AQD1D/vuD/wCO0f2nd/8AQD1D/vuD/wCO1d85/wDn2l/Nf8aPOf8A59pfzX/GgCl/ad3/ANAPUP8AvuD/AOO0f2nd/wDQD1D/AL7g/wDjtXfOf/n2l/Nf8aPOf/n2l/Nf8aAKX9p3f/QD1D/vuD/47R/ad3/0A9Q/77g/+O1d85/+faX81/xo85/+faX81/xoApf2nd/9APUP++4P/jtImq3TjK6HqGMkffg7HH/PWr3nP/z7S/mv+NQ28r+Wf9HlPzv3X+8fegCD+07v/oB6h/33B/8AHaP7Tu/+gHqH/fcH/wAdq75z/wDPtL+a/wCNHnP/AM+0v5r/AI0AUv7Tu/8AoB6h/wB9wf8Ax2j+07v/AKAeof8AfcH/AMdq75z/APPtL+a/40ec/wDz7S/mv+NAFL+07v8A6Aeof99wf/HaP7Tu/wDoB6h/33B/8dq75z/8+0v5r/jR5z/8+0v5r/jQBS/tO7/6Aeof99wf/HaP7Tu/+gHqH/fcH/x2rvnP/wA+0v5r/jR5z/8APtL+a/40AUv7Tu/+gHqH/fcH/wAdo/tO7/6Aeof99wf/AB2rvnP/AM+0v5r/AI0ec/8Az7S/mv8AjQBS/tO7/wCgHqH/AH3B/wDHaP7Tu/8AoB6h/wB9wf8Ax2rvnP8A8+0v5r/jWTresXNhCDawo0wyzRycnb6/K3H41nVqxpQc5bI0pU5VJqEd2WE1W6cZXQ9Qxkj78HY4/wCetPt9Uaa/Szm0+6tneJ5VaUxkEKVBHyO3Pziud8Mavql5I8IijkgRzJK5+8NxJwOR3zW87s/iWy3RsmLO4+8Rz88PoTWeGxEcRTVSK0LxOHlh6ns5PU1KKRmVFLMQqgZJJwAKq2uq6dfSNHaX9rcSL95Yplcj6gGugwLdFFc14g0K+1HWNPv7QWzfZCGCzPs5Dq3XYxGQuMqV99w+WgDpazPEbbPDGqHaT/okowB6qRWJc+Fbi4mvGZLZxIzsG8za1wWmWRPMzGwxGFKgEOCD/DVrUbC7sfh9PZLdIZ7fTykkrxlwwVDuAGR1xgHt6UAbU11BZpNLPKka78DewXJ2jgZ71wN1deHrnVXuXtLhvNcyvcE/vlkGNu054A5GP54r0FRKjPuXfuOcqAB0A7n2rFl0CG6vprxjP9rEwdXG3CYAIGM8jGK5MT9bVnhpW7/1+h1YdYR3WIjft/X6mtYajbalAJbeRW4BZNwLJnpkAnFULvX9CFx5F3MnmQPI6+bAxAaJdzFSVwSo9PWtXfJ/zyb9P8aw5fC1hPdG5liuHkNz9oO5wRncrbQCcAbkQnGCdoBJGQemKklaTuznk03eKsiWLxZpslxHCy3cZklliVnt3C7klERyccAswwenqQeKefFejLHvN0+0qXB+zycoBu3/AHfuYBO7ocHmnf2JY72f7HISztJzKSAzSLIcDfxl1DYHHX1NRL4c0tYnjFg5V4mhIMrHEZXaUGX4XBIAHAzxiqJNg/8AHwn+638xUlQETPIrLhMAj51znOPQ+1O23H/PWL/v2f8A4qgCWiottx/z1i/79n/4qjbcf89Yv+/Z/wDiqAJaKi23H/PWL/v2f/iqNtx/z1i/79n/AOKoAloqLbcf89Yv+/Z/+Ko23H/PWL/v2f8A4qgCWorb/VN/10f/ANCNG24/56xf9+z/APFVDbrceWcSxfff/lmf7x/2qALdFRbbj/nrF/37P/xVG24/56xf9+z/APFUAS0VFtuP+esX/fs//FUbbj/nrF/37P8A8VQBLRUW24/56xf9+z/8VRtuP+esX/fs/wDxVAEtFRbbj/nrF/37P/xVJtuP+esX/fs//FUATVHPPFbRGWaRUQd2NUDe3U8hjsTDOQcNKUIjX8c8n2H44p8WnSrMLie4Secfdd4zhP8AdG7A/n71l7Ry0hr59P8Ag/1qa+zUdZ6eXX/gB5l5ff6oNaW5/wCWjj94w9lP3fqefapl020W3eAwhkkOZNxJLn/aPU1IRcAZMsQH/XM//FVx/im9vdR0yR9HurgfZjuN1bFkRj02DBPmEnA4GAe46VpSw6nL3tfN/wBf15mdXEOEbx08kdPpsdk4e5tki3b5Ii6DH3XIx+GKZP8A8jLY/wDXncf+hw1wXgTSdZh1WSO/ur20WIeels27y5STgk4IBwcZHr1ru3Eg8S2XmMrf6HcY2rj+OH3Na1aUaUuWLuY0q0qseaSsQeJtN0u702S81ay+3QWMUk/2Z5MRyYGfmViEY/LwX4GT0ya4a61DRJbWymuPhfqNvHcOq288UFtDIjN93ayyKyMeg5BJIA5NekavFZXGi38OpFRYSW8iXJY4AjKkNk9uM15zplzHe6hpVvqur+JJdKFxE+nm+05IIriRTmLfIqhjyARuC7iB1PFZmp6Houo2uraNa3tm0pgkXA84ESKVO1lbPO4EEH3Bq/VPS9Ng0mxFpbFzH5kkpLnJLSOzsT/wJjVygArL8ShG8MaoJMY+yyHk452nH64rUrM8R7f+EY1TfjH2SXr67Tj9aANOoof9bcf9dB/6CtS1FD/rbj/roP8A0FaAJaKKKACiiigAooooAKKKKACiiigAooooAKitv9U3/XR//QjUtRW3+qb/AK6P/wChGgCWiiigAooooAKKjnnitojLNIqIO7GqfmXl9/qg1pbn/low/eMPYH7v1PPtUSqKLtu+xcYNq+yJ7m+it3EQDSzsMrDGMsff2HucCoPsc9781+4EXa2jPy/8CPVvpwPrVq2tIbRCsKYLHLMTlmPqSeSadPPDawPNPKkUSDLO7YAH1qfZyn8f3f1v+XkV7RQ+D7/62/rUeqqihUUKoGAAMAVSvtUgsnWHDzXTjMdvCNzt747D3OB71V+1X+rcWKtZ2h63UqfvHH/TND0/3m/I9avWOnW2nowgQ73OZJXJZ5D6sx5NdHKo7/cc/M5fD95RGm3Opnfq7qIeosYm+T/gbdX+nC+x61rqqogRFCqowABgAUtFJybHGKRFbf6pv+uj/wDoRqlP/wAjLY/9edx/6HDV22/1Tf8AXR//AEI1Sn/5GWx/687j/wBDhqSin4r1TRLTS5NO1uaaO31GGWAiKCSQspXa3KKccN3ri7DVLfU9Q0zT9Q8Xy31pDdQvDAmiTQSTSIwMfmSEFcBgpOAucc4Fd/q9vrEoil0e+t4JY87obmDzIpgcdSCGUjHBBxycg8YylXxzdHypn0HT4zwZ4DLcvj2VggB+pI9jQB1FFNjVkjVWcuwABdgMsfU44p1ABWZ4iUN4Z1UEAj7JKef9w1p1meI13+GNUG4j/RJTkH0UmgDTqKH/AFtx/wBdB/6CtS1FD/rbj/roP/QVoAlooooAKKKKACiiigAooooAKKKKACiiigAqK2/1Tf8AXR//AEI1LUVt/qm/66P/AOhGgCWiiq1zfRW7iIBpZ2GVhjGWPv7D3OBUykoq7HGLk7Is1Qa/adjHp8YmYHDSscRr+P8AEfYfmKT7HPefNfuBH2toz8v/AAI9W+nA+tX1VUUKqhVAwABgCo9+e2i/H/gf1sae5DfV/h/wf63KkFgqSie4c3FwOjuOF/3V6D+fvVyq17f22nxCS5lC7jtRQCWc+iqOSfYVQ8rUNX5uC9hZH/lijfvpB/tMPuD2Xn3HStYUlFaaL+vvMp1W3rq/6+4ludXAuGtLCE3l4vDKpwkX++/Rfpyfakg0gyTpdanMLu5U7kXbiKI/7C+v+0cn6dKvW1rBZ26wW0KRRL0RBgVNV81tIkct9ZBRRRUFhRRRQBFbf6pv+uj/APoRqlP/AMjLY/8AXncf+hw1dtv9U3/XR/8A0I1Sn/5GWx/687j/ANDhoAk1vUDpGg6jqSxGZrS1luBGDjeUUtj8cYrmJfGl42rxizgsp9KintLS6mWUlzNcEACPAwQu+MnPUN2xXaEBlIYAgjBBrn7W58HQGHTbSfQoytwHitYXhXE2eCqD+PPoM0AdDRRWHrK3p1Oxe1s76SOMmSWW3uVVcDpHsaRQxb1IOB7nIANyszxG2zwxqh2k/wCiSjAHqpFYp0/XTJrBc3UrSiUwZu/LQNvPkiPYwKgIRvzgkjjNaOt201t4LvLaC4LNDYuhluAZWcCMgknIJY+pJ59aANyoof8AW3H/AF0H/oK0bbj/AJ6xf9+z/wDFVDEtx5k/72L74/5Zn+6v+1QBboqLbcf89Yv+/Z/+Ko23H/PWL/v2f/iqAJaKi23H/PWL/v2f/iqNtx/z1i/79n/4qgCWiottx/z1i/79n/4qjbcf89Yv+/Z/+KoAloqLbcf89Yv+/Z/+Ko23H/PWL/v2f/iqAJaKi23H/PWL/v2f/iqNtx/z1i/79n/4qgCWiottx/z1i/79n/4qjbcf89Yv+/Z/+KoAlqnFd28Vo8zzIsYlkBYnvvIx9ax9fnuZbZ0srmQzQHLNbhlC/wCyTnkn0HP0rK8NaVef2luu3lhaHMkaOpKsScE9fXrXn1cZJVlShG9+vT+tzvp4SLourOVrdOv9bHU77y+/1Qa0tz/Gw/eMPYH7v1PPsKtW1pDaIVhTBY5Zicsx9STyTS7bj/nrF/37P/xVUL7UzZSLAHWe7cZS2hiJdvf72FHucCu2FLW+7/r7jjnV0tsv6+80yQASTgDqTWS2qz6gxi0eNZFBw15ID5K/7veQ/Tj3qL+y7/Utr6tPD5fX7FGpMf8AwM5Bf6fd9j1rVWOdFCrJCFAwAIiAB/31W2kfNmPvS8kV7LSobSU3Mjvc3jDDXE3LY9F7KPYYq/UW24/56xf9+z/8VRtuP+esX/fs/wDxVS23qykklZEtFRbbj/nrF/37P/xVG24/56xf9+z/APFUhktFRbbj/nrF/wB+z/8AFUbbj/nrF/37P/xVAEtFRbbj/nrF/wB+z/8AFUbbj/nrF/37P/xVABbf6pv+uj/+hGqU/wDyMtj/ANedx/6HDU9utx5ZxLF99/8Almf7x/2qrOJB4lsvMZW/0O4xtXH8cPuaAH6/aS3/AIc1SzgDmW4tJYkCOEYsyEDDHgHJ61ymh2up2x0+Gf4eaZaeWY1e4guYSIsEZdRtzx1xnPvXY3mmWWoZ+1W6y7oJLc7s/wCrkxvX6Hav5Vi2vgHwpaXcV1baNbpPBIsiOpbKMCCD1+hoA6WiisfWBepqOl3FnaXlwsUrmdYJ1VShjcAMrOoY7yhHBxjtQBsVmeImC+GdVJIA+ySjn/cNYf2HXP7Q1ZpUvXt5Q4gCXOMHeNhXEy9s5x5WBkHfnNXNRjvovh9LFcRrLdppxWfzpedwj+Y7gDlsjPue/egDo6ih/wBbcf8AXQf+grRuuP8AnlF/38P/AMTUMTXHmT/uovvj/lof7q/7NAFuiot1x/zyi/7+H/4mjdcf88ov+/h/+JoAloqLdcf88ov+/h/+Jo3XH/PKL/v4f/iaAJaKi3XH/PKL/v4f/iaN1x/zyi/7+H/4mgCWiot1x/zyi/7+H/4mjdcf88ov+/h/+JoAloqLdcf88ov+/h/+JqnLqE/nm3toI5px97Eh2x/7x28fTrUymoq7KjFydkXJ7iK1iMszhEHGT3PoPU+1VNl1qP8ArN9ran+AHEkg9z/CPYc/Skgs7hZRcXIjnuezlyFT2UY4/n71c3XH/PKL/v4f/iaz5ZT+LRdv8/8AIvmjD4dX3/y/zHRQxwRLFEioijAVRgCq63Vvb2UlxNNHHCkkm52YADDkdfrXOeLta1C0s3GlXKR3VuPMmCBZFVP9ssuFPoM5Poe3J+BYNW1HWc3Vx+7tD9o+z3A5YuSdygg456t154613U8Nenzt2SOGpibVFBK7Z6F9p1DVuLMPZWZ63MqfvXH+wh+6Pdvy71esdOttPjZbePDOcySMSzyH1ZjyTUu64/55Rf8Afw//ABNG64/55Rf9/D/8TWLlpZbG6jrd6sloqLdcf88ov+/h/wDiaN1x/wA8ov8Av4f/AImpKJaKi3XH/PKL/v4f/iaN1x/zyi/7+H/4mgCWiot1x/zyi/7+H/4mjdcf88ov+/h/+JoAloqLdcf88ov+/h/+Jo3XH/PKL/v4f/iaAJaKi3XH/PKL/v4f/iaN1x/zyi/7+H/4mgAtv9U3/XR//QjVKf8A5GWx/wCvO4/9Dhqe3a48s4ii++//AC0P94/7NVnMh8S2XmKq/wCh3GNrZ/jh9hQBNrMF3c6FqFvp8vk3sttIlvJnGyQqQpz7HBrzLQdOu1vJdJsfDOo6ajazaX3mTxhY4UjihEp35IdmZJF4znfk16Xrsk0Xh7UpLe6jtJktZWjuJfuwsEOHb2B5P0rz/wAPSaDdT6ZcC18cPdO8TCW6+3tEXyMM5z5ZTPJ/hx7UAeoUUUUAFZviH/kWtV/685v/AEA1pVm+If8AkWtV/wCvOb/0A0AaVRQ/624/66D/ANBWpaih/wBbcf8AXQf+grQBLRRRQAUUUUAFFFFABUc08VvE0s0ioi9WY1BcXwSU29uhnuf7inAX3Y9h+voKbDYkyrcXjieccrxhI/8AdH9TzWTm27Q1/Jf12/I0UEleen5/15/mMzdaj032tqe/SWQf+yj9fpVyCCK2iEUMaog6AVJWfe6qltMLW3ja6vWGRBGeg9XPRV9z+ANXClrfd/19yJnV0tsv6+8uXFxDawPPcSpFEgyzucACsrzL7WeIPMsbA/8ALUjE0o/2QfuD3PPoB1qW30p5p0u9VkW4uEO6OJRiKE/7I7n/AGjz6Y6VqVrdR21ZlZy30RWt9PtLW1FrDbxrDncVIzk5zk56nPOTzS28aYMmxd4Z13Y5xvPGasVFbf6pv+uj/wDoRqW2ykktiWiiikMKKKKACiiigAooooAKKKKACiiigCK2/wBU3/XR/wD0I1Sn/wCRlsf+vO4/9Dhq7bf6pv8Aro//AKEapT/8jLY/9edx/wChw0AReIpLKTR7/T7u5jhFzY3BYyRl1EYUB2KjGQN4yMjOa4jQ/Eb/AG3TrL/hYlnfDzY4vKOjlHmGQNu7dgE9M475rsfGDa7/AMIxfJ4dtkn1GWGSOMtceS0eUbDocEFg23AOB7isb+1PFGo3NpZTw6Npii5heaWLVjNKVV1ZownlLksBt69GNAHbUUUUAFZviH/kWtV/685v/QDWlWX4lz/wjGqYcJ/osnJ/3Tx+PT8aANSoof8AW3H/AF0H/oK1LUUP+tuP+ug/9BWgCWiiigAooqrc3ywyCCJDPckZESdh6sf4R9fwzUykoq7KjFydkTyyxwRNJK6oijJZjgCqXmXOocQ77a1P/LQjEjj/AGQfuj3PPsOtOisWklWe+cTSqcog/wBXH9B3Puf0q9WdpT30X4/1/XkXeMNtX+BFb20NrEI4UCL19yfUnufenSyxwxNLK6pGgyzMcAD1JqpfapDZOsCo9xdyDMdvFy7e57KvucCq8WlzXsq3OrukrKd0dqnMMR7E5++3uePQCuiMEl2RhKbb01Yz7Veax8tgWtbI9bt1+eQf9M1PQf7R/AHrWhZWFtp8Jit49oJ3OxJLO3qzHkn3NWaKHK+i2BRtq9woooqSgqK2/wBU3/XR/wD0I1LUVt/qm/66P/6EaAJaKKKACiiigAooooAKKKKACiiigAooooAitv8AVN/10f8A9CNUp/8AkZbH/rzuP/Q4au23+qb/AK6P/wChGqU//Iy2P/Xncf8AocNAC6/bz3fhzVLa1837RNaSxxeSwV95QgbSSADnGMkD3FcVoOlx20mmrL8KLexnjaMNdo1k/ksCP3gYPvOOuQN3HrXeamIW0m8W4immgMDiSKAMZHXachQvO4jgY5z0rzNNK0m41fSX0Hw74ktruG+hkeW8FzHCsQYF9/mPg/LnAHJOO2aAPVqKKKACsvxKQPDGqZQsPssnAGf4Tz+HWtSszxHu/wCEY1TZjP2SXr6bTn9KANOoof8AW3H/AF0H/oK1LUUP+tuP+ug/9BWgCWmu6RozyMFRRksxwAKgub2O3cRKrS3DDKwp94+59B7moUspLh1mv2WRgcpCv+rQ/wDsx9z+AFZyqa8sNX+Rooac0tEJ59xqHFrugtj1nYfM/wDuA9PqfwHerVtaw2keyFMZOWJOSx9SepNTVTvtTt7DYr7pJ5P9VBEN0kh9h6e5wB3NOFLW71f9bdhTqaWWiLbMqKWZgqgZJJwAKyDf3WrEppWIrbo1865B/wCuan73+8fl/wB6hdNuNSYS6uV8oHKWMZzGP98/xn2+6PQ9a2AABgDAFbaR82Y6y8kVLHTrfT0YQqxkkOZJXO55D6sx6/07VbooqW23dlJJKyCiiikMKKKKACorb/VN/wBdH/8AQjUtRW3+qb/ro/8A6EaAJaKKKACiiigAooooAKKKKACiiigAooooAitv9U3/AF0f/wBCNUp/+Rlsf+vO4/8AQ4au23+qb/ro/wD6EapT/wDIy2P/AF53H/ocNAGL4+1A2OnabG+oXWn2l1frDd3VoD5kcflSP8pAJGWRVyB3rkvCep6JqM1rFrviHUdTu7e/ePToblJVXCzEQyOFUB3I2nc+cZHA5Nes0UAFZOo601hqENstsJEYxea5k2lRJKI12jB3cnJ5GB65rWqtdadZXxU3dnb3BQEKZolfaD1AyO9AHP3PjSGO+1G0toI7iSzMKKBMS0jNIyONiqzAKV7Ak+mME2NY1OO48BXF+7RW4ubAuFkkGAXTO3Pc84Hqa2biws7sOLm0gmEgVXEkYbcFJKg564JJHpmqOvrHbeE9SjiiCxpZyIiRrgKNhAwOwH8qALN7qttZWzTs3mqvVYiGbHrjNc1D4sN1ey28KPGs8o2SBQWVdoHQnGeM5JwPeuwlUvE6K21mUgNjOD61i6V4fttNvmnt5JAyZjcNg7wVB/DmuHE08TOpH2UrR6nbhp4eNOXtFeXQ0LYWNohEc0e5jl3aQFnPqT3qb7Xbf8/EX/fYqO+1G30+NWnY73O2OJBueQ+iqOSaofYbvVzv1T9za/w2KNnd/wBdWHX/AHRx67q74U0l2RwzqNvuxkutm/ka30mWHaDh7yU/u1/3B/Gf09+1WrG2sLDfItwstxJ/rbiWQNI/1PYewwB6VooixoqIoVVGAoGABS03LSy2JUdbvci+1W//AD3i/wC+xR9qt/8AnvF/32KloqSyL7Vb/wDPeL/vsUfarf8A57xf99ipaKAIvtVv/wA94v8AvsUfarf/AJ7xf99ipaKAIvtVv/z3i/77FH2q3/57xf8AfYqWigCL7Vb/APPeL/vsVDb3NuIzmeL77/xj+8at1Fbf6pv+uj/+hGgA+1W//PeL/vsUfarf/nvF/wB9ipaKAIvtVv8A894v++xR9qt/+e8X/fYqWigCL7Vb/wDPeL/vsUfarf8A57xf99ipaKAIvtVv/wA94v8AvsUfarf/AJ7xf99ipaKAIvtVv/z3i/77FH2q3/57xf8AfYqWigCL7Vb/APPeL/vsUfarf/nvF/32KlooAqW9zbiM5ni++/8AGP7xqs8scviWy8uRXxZ3Gdpzj54av23+qb/ro/8A6EapT/8AIy2P/Xncf+hw0AaVFFFABWde6zBY3sNq8UztIUDOgG2Pe4RC2SDyxxwD0OcVo1RvtHstSkSS5jcugwGSZ4z1BGdpGcEAjPQ8jBoArya9Gl9eWa2dxJNaxiRlWSLlSevLjb6/NtyM4zg03VblL3wbfXUauqT6fJIocYYBoyRkdjzUl54e02/Mxuo55DKMNm6lGBuDYXDfKCVGQMA4Gah8TWkL+ENRhkVXjitHYGYeZjap5JbPPHU896AL+o6laaTZtd3spigUgM+xmxn1wDXHx/EG0udQuLKyKh5bgLBPIjFCu1RnaBuJznA49yK3rnVPDs9pPFHqGjMzIygPNGVzjvz0rk/Duh6Bomrpd/2/pdyEUxyrLJH1IB3Jzx6fTNdFL2PK3Pfoc1b23MlDbqd3Y6XFZyNcO73F44w9xLyxHoOyr7Dir1ZH9r+HP+ghpX/f6P8Axo/tfw5/0ENK/wC/0f8AjWDberOhJJWRr0VjnWPDajJ1HSgPUzx/40v9r+HP+ghpX/f6P/GkM16KyP7X8Of9BDSv+/0f+NH9r+HP+ghpX/f6P/GgDXorHGseGySBqOlZHUefHx+tL/a/hz/oIaV/3+j/AMaANeisj+1/Dn/QQ0r/AL/R/wCNINY8NnONR0o4ODieP/GgDYorI/tfw5/0ENK/7/R/40h1jw2CAdR0rJ6Dz4+f1oA2Kitv9U3/AF0f/wBCNZv9r+HP+ghpX/f6P/GoYNY8OCM7tS0oZkcDM8f94+9AG7RWR/a/hz/oIaV/3+j/AMaQ6x4bBAOo6Vk9B58fP60AbFFZH9r+HP8AoIaV/wB/o/8AGj+1/Dn/AEENK/7/AEf+NAGvRWR/a/hz/oIaV/3+j/xo/tfw5/0ENK/7/R/40Aa9FZH9r+HP+ghpX/f6P/Gj+1/Dn/QQ0r/v9H/jQBr0Vkf2v4c/6CGlf9/o/wDGj+1/Dn/QQ0r/AL/R/wCNAGvRWR/a/hz/AKCGlf8Af6P/ABo/tfw5/wBBDSv+/wBH/jQBpW3+qb/ro/8A6EapT/8AIy2P/Xncf+hw1Vt9X8O+Wc6jpf33/wCW8f8AePvTYLzTbrxNaDT7m0mK2dxvFu6tj54cZx+NAG9RRRQAUUUUAFFFFAGXq+qXGmPa+VapMk8qwlmkZNrMwVckIQBk9yPQZJAOVH4mET6Yy6aofVBHLOVlYiNmZIxkhMemC2zO3AyeK6C40+yvJEkubO3nkThGliViv0JHFY9/N4V0C70ezvLewtJbiYxaev2UYEmQcKQuEOcemTigDoaKqw6laT6ndadHLuu7WOOSaPaRtWTdsOcYOdjdD25q1QAUUVn6jq9vpl5pttMkrPqFwbaIoAQrCN5Mtk9MIemecUAaFFFFABRRRQAUVk6Zr8WralfWttZ3fk2cjQveOEETSKQGRfm3EjPXbjg803VPFGkaPeR2d3cubt08xbe3gknk25xuKRqxAz3IxQBsUVV0/UbTVbNbqym82FiRnaVIIOCCCAQR6EVIkztdywm3lVEVWExK7HJzkDBzkYGcgDkYJ5wAZXiTXpvD9slytmlzCdxcecVcBUZ2KrtIOFRjyRyAO9Z9t4naG4t7I6T5DtMRdgTZW3Z5Qo5x85ZnB46Z6+mvPqFi/iODR5rYyXb2ctykjIpURhkRlyTnJLLxjBAqjqt/4a8PXGnW13ZKszeZJZxWumyTsu0qXZREjFeWXJ460AdFRWKniiwluNKijivM6nNJDCZbZ4SrIjOdyyBWAwpxxzx25raoAKKKKACiimyOsUbSOcKoLE+gFADqKr6ffW2qadbahZyeba3MSzQybSu5GGQcHBHB71YoAKKwb3xZYWVrql00VxJDps8dvM8aqQ0j7eFyRnbvXPT05INb1ABWDqmvXlg+pJHYQTNaQxTRhrkqZhIXUKPkOG3JgDkHcOR0rerGnOjweI4LebToRfX0ZkS5MCfvDEVO0t13DIYA/wB0kdKAKA8SFb+/0vSNNhu7q2USmBLtULu0mJeo4ClicnqcjA4J39OvF1DTbW9QYWeJZAM9MjOOg/lS3NhZ3gYXVpBOGUKwljDZAOQDntnmpo40ijSONFSNAFVVGAoHQAelADqKKKACiiigAooooAK4bxxpNvrviTw3pl1uEVwt4pZeGQ+UCrD0IIBB9QK7migDxVrnUtZsfiDb3du7avZ6ZZW90kaH95JG05LoO4ZcOB/tYrb1TXdN13xTqE2l3SXUCeFrwGaPlMl04B9R3HbIzXp9FAHk+n+GNJS/8DxC1Hl6hpkn29SxIvNsUbL5v9/DHIB+nTiqF4LCHStFs9QLjSbPxZeW4QFiI4FW4wpxzsA4PbbnPFez0UAeO3W46Brg8Nsq+Fzq1ttZI3kgWHav2goqEMYt+NwUgffxxmoLqztB4L8Tf2brel3NnL9jU2+jQPDDA/nAb1zI4DMMZCkfdBIz19pooApabpNho1o1tptpFbRFi5WNcbmIGWPqTgZJ5NeVaB9g8zw19k3f8Jp9uH9sdfP24bz/ADv9j+7nj7u2vYqKAOH+HOjaXpya7NZada20n9r3UG+GFVPlrJ8qZA+6Ow7VVTULTw34w8VjW77+y21QwyWWoygBGQQhNqswKbkYMdrf3gcHJr0KigDyaZ7jxHpvh6LVbue/tn8UvFDcSRiI3FusE+0/IqgqQCMgYIJqbXLNtPvPGdposDW6RaRp6rFZrtZYvNn8wIF6HZvxjn0r1OigDzHwx/wjX/C0of8AhFvJ+w/2HLu+zZ8nf50Wcdt+Mbu/TPNXfHV1b2fjfwxLc+IBocf2S/H2stEOc2/y/vVZefpnivQaKAOBe7t7zW/BElrra6zGL67H20NEd5+zS8fuwF46cCuT8NWzTanpkmoa3pFl4kS+DXcX2OUX8h3nfGzGX5o2GQDt2AEEAYr2qigDxqfRLI+HtR1ZUdNS/wCEskiS7SQrJGj3/lsqsDlVKs2QO5z1rQ1iyn0dvG2meG4WtoxZWFwILdWwu+SRZmVVIOTGnO0gkjg55r1WigDxlLawj8O+LJtL1vRJ4G8PXay2WkWjwpu2HEj5kcBx8w7E7uelbf8AYGn6fr2iWdlaIE1XRbyO+Rvm+1lVhKmTP3m+ZuTz8xFel0UAeM6fa+FH+G2gxw3+h6fPB9nbUYbmMeTPOIWUx3QUqQcksN3dehr0PwLdQ3fhaF7fTobCBJZY0jtyTC4Dkb4yQDsbqOB1ro6KAPLb0GP4ZeIoJf8Aj5i1uUT+pLXqup/FHQj2Iqr4n/sn7f4t/tvf/wAJBkf2F97zdvkr5X2bH8Xm7923v14r1WKzt4Lm4uYolSa4KmZx1cqMAn3xx+A9KnoA8m13+zf7W1v/AIS/H9o/2bD/AGXnO7PlHzPIx/y083OdvONvatOxBl0D4XQQ/wDHyBBNkdREtk4c/T5lH1YV1mt+Hf7cLLJq+p2ttJF5U1tayIqSrznJKFgSDjKkcVfg0uxtpLeSG2RHtoPs0JA/1cXHyj0Hyr+Q9KALdFFFAH//2Q==",
      "text/plain": [
       "<Figure size 320x240 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot(results[0],results[1])"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
