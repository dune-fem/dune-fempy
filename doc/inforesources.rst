.. title:: Information and resources

.. toctree::
   :maxdepth: 1

   citing
   developers
   contributions
   additional
   changelog


#############
Keyword index
#############

.. 

* :ref:`genindex`
